/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import * as React from 'react';
import { LoginStyle as styles } from './styleSheet/LoginStyle';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { useState,useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import action from './actions/index';


// import  {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity
  
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { color } from 'native-base/lib/typescript/theme/styled-system';

import { Icon,NativeBaseProvider, Box ,Item,Label} from 'native-base';


const LoginScreen = ({ navigation }) => {

  const dispatch = useDispatch()
  let token = useSelector((state) => state.appReducer.token);
  const isAuthenticated = useSelector((state) => state.appReducer.isAuthenticated)
  const loginError = useSelector((state) => state.appReducer.loginError)
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState(null);
  const [error,setError] = useState(false)
  console.log('token==========>',token)
  console.log('isAutenticated========================>',isAuthenticated)
  console.log('loginError',loginError)
  useEffect(() => {
    if(isAuthenticated === true && token !== null){
      navigation.navigate('Option')
      setError(false)
    }
   setEmail("");
   setPassword("")
  
  },[isAuthenticated])

  const Login = () =>{
    dispatch(action.initiateLogin({ email: email, password: password }))
    if(isAuthenticated&& token !== null){
      setError(false)
      navigation.navigate('Option')
    }
    if(isAuthenticated === false){
      setError(true)
    }
  console.log('loginError1',loginError)
      
  }
 
  return (
    <ScrollView style = {styles.container} showsVerticalScrollIndicator={false}>

   <ImageBackground source = {require('./images/deep-blue-background.jpg')}
   style= {styles.imageStyle}>
     <View>
        <FontAwesome5 style = {styles.iconStyle} name={"tools"} size ={100} color="white" />
        <Text style = {styles.textStyle}> MiDAS Installation</Text>
     </View>
     {/* <Image source = {require('./images/insta2.png')} style = {styles.iconStyle} /> */}
   </ImageBackground>
   <View style={styles.bottomView}>
     <View style={{padding:40}}>
       <Text style={{color:'#4632A1',fontSize:34}}>Welcome</Text>
       <Text style ={{color:"black",fontSize:15}}>Login In and check the live data!</Text>
     <View style={{marginTop:60}}>
       {/* <Item floatingLabel style={{borderColor:'#4632A1'}}>
        <Label>Email</Label>
        <Input value='design@test.com' keyboardType='email-address'/>
       </Item> */}
        <SafeAreaView>
      <TextInput
        style={{height: 40,backgroundColor: 'azure', fontSize: 20}} 
        onChangeText={setEmail}
        placeholder="email"
        value={email}
      />
      <TextInput
        // style={styles.input}
        style={{height: 40,backgroundColor: 'azure', fontSize: 20,marginTop:15}} 
        secureTextEntry = {true}
        onChangeText={setPassword}
        value={password}
        placeholder="password"
      />
    </SafeAreaView>
    <View style={{marginTop:35}}>
           <TouchableOpacity style = {[styles.button,styles.shadowOffset,{shadowColor:'#00acee'}]}  onPress={()=>{Login()}}>
                        <View style = {{flexDirection: 'row'}}>
                            <Text  style = {{...styles.subtitletext2, ...styles.fontColor2}}>Login</Text>
                        </View>
          </TouchableOpacity>
    </View> 
    <Text style={styles.ErrorStyle}>{isAuthenticated === false ? loginError.message : null}</Text>               
     </View>
     </View>
   </View>
    </ScrollView>
  );
};


export default LoginScreen;
