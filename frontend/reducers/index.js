import ACTIONS_TYPES from "../actions/type";
import { combineReducers } from 'redux';
import AsyncStorage from '@react-native-community/async-storage';


const defaultState = {
    login: false,
    loading: false,
    isAuthenticated: false,
    loginError: { type: '', message: '' },
    token: "",
    user: {},
    deviceNumber:"",
};

const appReducer = (state = defaultState, action) => {
    // console.log("state = ", state)
    console.log("action = ", action)
    switch (action.type) {
        case ACTIONS_TYPES.SET_USER_DETAILS: {
            return {
              ...state,
              user: action.payload,
            };
          }
          case ACTIONS_TYPES.LOGIN_SUCCESS: {
            return {
              ...state,
              isAuthenticated: true,
            };
          }
          case ACTIONS_TYPES.SET_TOKEN: {
            return {
              ...state,
              token: action.payload,
            };
          }
          case ACTIONS_TYPES.LOGIN_ERROR: {
            return {
              ...state,
              loginError: action.payload,
            };
          }
          case ACTIONS_TYPES.SIGN_OUT_SUCCESS: {
            return {
              ...state,
              isAuthenticated: false,
              token: "",
            };
          } 
          case ACTIONS_TYPES.GET_DEVICE_ID: {
            return {
              ...state,
              deviceNumber:action.payload
            }
          } 
       
       
       
        default: return state
    }
}

export default combineReducers({
    appReducer
})
