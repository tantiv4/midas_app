/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import * as React from 'react';
import { LoginStyle as styles } from './styleSheet/LoginStyle';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import WebView from 'react-native-webview';
import { useSelector, useDispatch } from "react-redux";
import AsyncStorage from '@react-native-community/async-storage';
import { ActivityIndicator} from "react-native";



// import  {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity
  
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { color } from 'native-base/lib/typescript/theme/styled-system';

import { Icon,NativeBaseProvider, Box ,Item,Label} from 'native-base';


const WebScreen = ({ route,navigation}) => {
  const dispatch = useDispatch()
  let token = useSelector((state) => state.appReducer.token);
  console.log('token1============>',token)  
  const INJECTED_JAVASCRIPT = `(function() {
    const tokenLocalStorage = window.localStorage.setItem('id_token',token);
    window.ReactNativeWebView.postMessage(tokenLocalStorage);
  })();`;
  let myInjectedJs = `(function(){ let tk = window.localStorage.getItem('id_token');
      if(!tk || (tk && tk != '${token}')){
        window.localStorage.setItem('id_token', '${token}');
        window.location.reload();
      }
    })();`;
    const { id } = route.params;
    console.log("id==========>",id);
    
    const signOut = () => {
      AsyncStorage.removeItem('id_token', null)
      dispatch({ type: "SIGN_OUT_SUCCESS" });
      dispatch({ type: "LOGIN_ERROR", payload:{type:'success'}});
      
    }

  const  IndicatorLoadingView = () => {
      return (
        <ActivityIndicator
          color="#3235fd"
          size="large"
          style={styles.IndicatorStyle}
        />
      );
    }
  return  <View>
    <View style={styles.webViewStyle}>
    <WebView  source={{ uri: `https://poweriot.tantiv4.com/installation?query=${id}`,
//   headers: {
// "id_token":token  } 
}}
ref={webView => { this.refWeb = webView; }}  javaScriptEnabled={true}    injectedJavaScript={myInjectedJs} renderLoading={IndicatorLoadingView}
startInLoadingState={true}/>
</View>
<View>
<TouchableOpacity style = {[styles.LogOutStyle,styles.shadowOffset,{shadowColor:'#00acee'}]} onPress={() => {signOut();navigation.navigate('Login')}} >
                        <View style = {{flexDirection: 'row'}}>
                            <Text  style = {{...styles.subtitletext2, ...styles.fontColor2}}>LOG OUT</Text>
                        </View>
</TouchableOpacity>
</View>
</View>
   
};


export default WebScreen;