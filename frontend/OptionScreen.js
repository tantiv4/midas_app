/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import * as React from 'react';
import { LoginStyle as styles } from './styleSheet/LoginStyle';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import { useState ,useEffect} from "react";
import { useSelector, useDispatch } from "react-redux";
import action from './actions/index';


// import  {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  Modal,
  Alert,
  Pressable
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { color } from 'native-base/lib/typescript/theme/styled-system';

import { Icon,NativeBaseProvider, Box ,Item,Label} from 'native-base';


const OptionScreen = ({ navigation }) => {
  const dispatch = useDispatch()

  const [deviceId, setDeviceId] = useState("");
  const [serialNo, setSerialNo] = useState("");
  const [open,setOpen] = useState(false);
  const deviceNumber= useSelector((state => state.appReducer.deviceNumber))

  
 const  onSuccess = (e) => {
    let scanId = e.data.split("//")[1]
    let regex = /^[a-zA-Z0-9]+$/i ; 
    console.log('e.data=========>',scanId)
    if(e.hasOwnProperty('data') && regex.test(scanId)){
      setSerialNo(scanId)
      setOpen(false)
    }
    else {
        setOpen(false)
        Alert.alert('Alert',
            'Invalid Code.',
            [{text: 'OK', onPress: () => console.log('OK Pressed')},
            ],{cancelable: false})
    } 
}

const continueFunc = () => {
  dispatch(action.getDeviceID(serialNo))
  setDeviceId(deviceNumber)
 navigation.navigate('Web',{id:deviceNumber})
}
 
useEffect(() => {
  // dispatch(action.getDeviceID('T4MDS00005'))
  // console.log('deviceNumber',deviceNumber)
 
}, [dispatch])
  return (
    <ScrollView style = {styles.container} showsVerticalScrollIndicator={false}>

   <ImageBackground source = {require('./images/deep-blue-background.jpg')}
   style= {styles.imageStyle}>
     <View>
        <FontAwesome5 style = {styles.iconStyle} name={"tools"} size ={100} color="white" />
        <Text style = {styles.textStyle}> MiDAS Installation</Text>
     </View>
     {/* <Image source = {require('./images/insta2.png')} style = {styles.iconStyle} /> */}
   </ImageBackground>
   <View style={styles.bottomView}>
     <View style={{padding:40}}>
       <Text style={{color:'#4632A1',fontSize:34}}>Welcome</Text>
       <Text style ={{color:"black",fontSize:17}}>Enter device Id or scan the QR code to get the deviceId!</Text>
     <View style={{marginTop:60}}>          
     </View>
    
     <SafeAreaView>
      <TextInput
        // style={styles.input}
        style={{height: 40,backgroundColor: 'azure',color:'black', fontSize: 20,marginTop:15}} 
        onChangeText={setSerialNo}
        value={serialNo}
        placeholder="Enter Serial No."
      />
    </SafeAreaView>
    <View><Text style={{...styles.subtitletext2,...styles.orStyle,color:'black',fontSize:15}}>OR</Text></View>
     <View style = {styles.optionButton}>
     <TouchableOpacity style = {styles.scannerButton} onPress={()=>setOpen(!open)}>
                    <Text  style = {styles.qrFont}>Scan for QR code</Text>
                </TouchableOpacity>
                </View>
                <View style={{marginTop:15}}>
     <TouchableOpacity style = {[styles.button,styles.shadowOffset,{shadowColor:'#00acee'}]} onPress={() =>
         continueFunc()
      } >
                        <View style = {{flexDirection: 'row'}}>
                            <Text  style = {{...styles.subtitletext2, ...styles.fontColor2}}>Continue</Text>
                        </View>
          </TouchableOpacity>
          </View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={open}
                onRequestClose={ () => {setOpen(!open)}} >
                <View style = {{flex : 1,backgroundColor:'white'}}>

                    <QRCodeScanner
                        onRead={onSuccess}
                        showMarker = {true}
                        topContent={
                        <Text style={styles.centerText}>
                            Scan the QR code and get Device Id.
                        </Text>
                        }
                        bottomContent={
                        <TouchableOpacity style={styles.buttonTouchable} onPress = { () => setOpen(!open)}>
                            <Text style={styles.buttonText}>Cancel</Text>
                        </TouchableOpacity>
                        }
                        // ref={(node) => { open = node }}
                        reactivate = {true}
                        reactivateTimeout = {5000}
                    />
                   
                    
                </View>
            </Modal>
    
     </View>
   </View>
    </ScrollView>
  );
};


export default OptionScreen;
