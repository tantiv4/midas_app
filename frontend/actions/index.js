import CONSTANTS from "../constants/index";
import TYPES from "./type";
import api from "../api/index";
import { getApi, postApi, patchApi } from "../api/index";
import { useSelector } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
// import AsyncStorage from '@react-native-async-storage/async-storage';



// Login Actions

const initiateLogin = (data) => async (dispatch) => {
    try {
      console.log(data);
      const resp = await postApi(data, `/server/auth/login`);
      console.log("You are here", resp);
      if (resp.type === "success") {
        if (resp.user) {
          AsyncStorage.setItem("userDetailVal", JSON.stringify(JSON.stringify(resp.user)));
          AsyncStorage.setItem("id_token", resp.token);
          // AsyncStorage.setItem("isAuthenticated",true)
          dispatch({ type: TYPES.SET_USER_DETAILS, payload: resp.user });
          // dispatch({ type: TYPES.SET_SIDEBAR, payload: CONSTANTS.structureList.filter(el => resp.user.acl.includes(el.label)) })
          dispatch({ type: TYPES.LOGIN_SUCCESS });
          dispatch({ type: TYPES.SET_TOKEN, payload: resp.token });
        }
        dispatch({ type: "LOGIN_SUCCESS", payload: { type: 'success' } });
        // return history.replace("/app/dashboard");
      } else {
        // setError(true)
        //Dispatch Login Error
        dispatch({
          type: TYPES.LOGIN_ERROR,
          payload: { message: resp.msg, type: resp.type },
        });
      }
    } catch (e) {
      console.log(e, "Login fail!!");
      dispatch({ type: TYPES.API_ERROR, payload: { message: e.message } });
    }
  };
  const getDeviceID = (scanNumber) => async (dispatch) => {
    try {
      console.log('scanNumber',scanNumber)
  
      //  dispatch({ type: TYPES.CHECK_EMAIL_ID, payload: { loading: true } });
      let response = await getApi(`/asset/getDeviceId?scanNumber=${scanNumber}`);
      console.log('getDevice_Response',response.status)
      if (response.type === "success") {
        dispatch({
          type: TYPES.GET_DEVICE_ID,
          payload: response.status,
        })
      }
  
    } catch (e) {
      console.log("GET DEVICE_ID ERROR ", e);
      dispatch({ type: TYPES.API_ERROR, payload: { message: e.message } });
    }
  };
  
  
  export default {
    initiateLogin,
    getDeviceID
  }
