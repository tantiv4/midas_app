/*
* Copyright (c) Tantiv4 Inc.
* All Rights Reserved.
* Confidential and Proprietary - Tantiv4 Inc.
*/

/*
 * style sheet for login screen 
 */

import { StyleSheet, Dimensions , Platform} from 'react-native';
const { width: windowWidth, height: windowHeight } = Dimensions.get("window");

const LoginStyle = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#ffffff'
    },
    imageStyle:{
        height:windowHeight/2.3
    },
    iconStyle:{
         marginTop:windowHeight*.1,
         marginLeft: windowWidth *.37
    },
    textStyle:{
      color:'white',
      fontSize:20,
      fontWeight:'bold',
      marginTop:windowHeight*.02,
      marginLeft: windowWidth *.28,

    },
    bottomView:{
        flex:1.5,
        backgroundColor:'white',
        bottom:50,
        borderTopStartRadius:60,
        borderTopEndRadius:60
    },
    button:{
        marginVertical: windowHeight*0.01,
        height: windowWidth*0.1,
        backgroundColor:  '#4632A1',
        width : windowWidth * 0.8,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
         borderRadius: 25,
    },
    subtitletext2:{
        fontWeight: '700',
        fontSize : parseInt(windowHeight*0.021)
    },
    fontColor2: {
        color: "#fff"
    },
    shadowOffset:{
         shadowOffset:{width:1,height:10},
         shadowOpacity:0.4,
         shadowRadius:3,
         elevation:15
    },
    optionButton:{
    //   marginTop:windowHeight*.01,
    //   marginLeft: windowWidth *.1
    },
    orStyle:{
      marginTop:windowHeight*0.030,
      alignSelf:'center'
    //   marginLeft:windowHeight*0.08
    },
    scannerButton:{
        marginVertical: windowHeight*0.02,
        height: windowHeight*0.06,
        width : windowWidth * 0.8,
        borderColor:'#4632A1',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth : 1.5,
    },
    qrFont: {
        fontWeight: '600',
        color: 'black',
        fontSize : parseInt(windowHeight*0.018)
    },
    buttonTouchable: {
        padding: 16,
        marginTop : windowHeight*0.05,
    },
    buttonText: {
        fontSize: 21,
        color: 'rgb(0,122,255)',
    },
    centerText: {
        flex: 1,
        fontSize: 18,
        padding: 32,
        color: '#777'
      },
      centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
      },
      button1: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#2196F3",
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center"
      },
      webViewStyle:{
        height: windowHeight/1.12,
      },
      LogOutStyle:{
        // marginVertical: windowHeight*0.01,
        height: windowWidth*0.15,
        backgroundColor:  '#6495ed',
        width : windowWidth * 1,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        //  borderRadius: 25,
    },
    ErrorStyle:{
     color:'red',
     fontWeight:'bold',
     marginTop:windowHeight*.02,
     marginLeft:windowHeight*.18
    },
    IndicatorStyle: {
      position: "absolute",
      alignItems: "center",
      justifyContent: "center",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0
    }
});

export {  LoginStyle }