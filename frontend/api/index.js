/*
* Copyright (c) Tantiv4 Inc.
* All Rights Reserved.
* Confidential and Proprietary - Tantiv4 Inc.
*/

/*
 * 
 * api method
 * 
*/
import CONSTANTS from '../constants/index'
 import AsyncStorage from '@react-native-community/async-storage';
 
// import AsyncStorage from '@react-native-async-storage/async-storage';

const postApi = async (body, path) => {
    let token = await AsyncStorage.getItem('id_token')
    console.log("token111111111111111", token)
    if (token || true) {
      const rawResponse = await fetch(`${CONSTANTS.BASE_PATH}${path}`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'jwt-token': token,
          'time-offset': -new Date().getTimezoneOffset(),
        },
        body: JSON.stringify(body),
      })
      return await rawResponse.json()
    }
  
    throw new Error('INVALID_TOKEN')
  }

  const getApi = async (path) => {
  let token = await AsyncStorage.getItem('id_token');
    console.log('token==============================================>',token)
    if (token || true) {
      const rawResponse = await fetch(`${CONSTANTS.BASE_PATH}${path}`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'jwt-token': token,
          'time-offset': -new Date().getTimezoneOffset(),
        },
      })
      return await rawResponse.json()
    }
  
    throw new Error('INVALID_TOKEN')
  }
  
  const patchApi = async (body, path) => {
    let token = AsyncStorage.getItem('id_token')
    if (token) {
      const rawResponse = await fetch(`${CONSTANTS.BASE_PATH}${path}`, {
        method: 'PATCH',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'jwt-token': token,
          'time-offset': -new Date().getTimezoneOffset(),
        },
        body: JSON.stringify(body),
      })
      return await rawResponse.json()
    }
  
    throw new Error('INVALID_TOKEN')
  }
  
export {
    postApi,
    getApi,
    patchApi
}