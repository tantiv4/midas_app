/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


import LoginScreen from './LoginScreen';
import OptionScreen from './OptionScreen';
import WebScreen from './WebScreen'

// import  {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';


const Stack = createNativeStackNavigator();

const App = () => {
 

  return (
   <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options = {{
            headerShown : false
        }}
        />
        <Stack.Screen 
        name="Option" 
        component={OptionScreen}
        options = {{
          headerShown : false
      }} />
       <Stack.Screen 
        name="Web" 
        component={WebScreen}
        options = {{
          headerShown : false
      }} />
      </Stack.Navigator>
   </NavigationContainer>  
  );
};


export default App;