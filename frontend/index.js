/**
 * @format
 */
import * as React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { Provider } from 'react-redux';
import store from './store';

//redux
const ReactNativeRedux = () => (
    <Provider store = { store}>
        <App />
    </Provider>
)

AppRegistry.registerComponent(appName,() => ReactNativeRedux);


